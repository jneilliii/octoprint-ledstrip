/*
 * View model for OctoPrint-Ledstrip
 *
 * Author: Paul H.
 * License: MIT
 */
$(function() {
    function LedstripViewModel(parameters) {
        var self = this;

        // assign the injected parameters, e.g.:
        // self.loginStateViewModel = parameters[0];
        // self.settingsViewModel = parameters[1];
        self.controlViewModel = parameters[0];

        // TODO: Implement your plugin's view model here.
        self.getAdditionalControls = function() {
        return [
        {name: "Light", type: "section", layout: "vertical", children: [
          {type: "javascript", javascript: "OctoPrint.control.sendGcode('M117 On');", name: "On"},
          {type: "javascript", javascript: "OctoPrint.control.sendGcode('M117 Off');", name: "Off"}
        ]}
        ];
        }
    }

    /* view model class, parameters for constructor, container to bind to
     * Please see http://docs.octoprint.org/en/master/plugins/viewmodels.html#registering-custom-viewmodels for more details
     * and a full list of the available options.
     */
    OCTOPRINT_VIEWMODELS.push({
        construct: LedstripViewModel,
        // ViewModels your plugin depends on, e.g. loginStateViewModel, settingsViewModel, ...
        dependencies: ["controlViewModel"],
        // Elements to bind to, e.g. #settings_plugin_ledstrip, #tab_plugin_ledstrip, ...
        elements: [ /* ... */ ]
    });
});
