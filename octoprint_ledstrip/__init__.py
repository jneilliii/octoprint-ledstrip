# coding=utf-8
from __future__ import absolute_import


import octoprint.plugin


import fakepi as pigpio

pi = pigpio.pi()


class LedstripPlugin(
    octoprint.plugin.StartupPlugin,
    octoprint.plugin.TemplatePlugin,
    octoprint.plugin.EventHandlerPlugin,
    octoprint.plugin.SettingsPlugin,
    octoprint.plugin.AssetPlugin
):
    def get_assets(self):
        return dict(
           js=["js/ledstrip.js"]
        )

    def on_after_startup(self):
        self._logger.info("started lestrip plugin")
        self.printing = False
        pi.set_PWM_dutycycle(self._settings.get(["pin"]), 0)

    def get_settings_defaults(self):
        return dict(pin=2)

    def on_event(self, event, payload):
        print(event, payload)
        if event == "PrinterStateChanged":
            if payload["state_string"] == "Printing":
                pi.set_PWM_dutycycle(self._settings.get(["pin"]), 255)
                self.printing = True
            elif payload["state_string"] == "Finishing":
                pi.set_PWM_dutycycle(self._settings.get(["pin"]), 0)
            elif payload["state_string"] == "Cancelling":
                pi.set_PWM_dutycycle(self._settings.get(["pin"]), 0)
                self.printing = False
        elif event == "ClientOpened":
            pi.set_PWM_dutycycle(self._settings.get(["pin"]), 255)
        elif event == "ClientClosed":
            if not self.printing:
                pi.set_PWM_dutycycle(self._settings.get(["pin"]), 0)

    def get_template_configs(self):
        return [dict(type="settings", custom_bindings=False),
                dict(type="controls", custom_bindings=False)]


# If you want your plugin to be registered within OctoPrint under a different name than what you defined in setup.py
# ("OctoPrint-PluginSkeleton"), you may define that here. Same goes for the other metadata derived from setup.py that
# can be overwritten via __plugin_xyz__ control properties. See the documentation for that.
__plugin_name__ = "Ledstrip Plugin"
__plugin_implementation__ = LedstripPlugin()

# Starting with OctoPrint 1.4.0 OctoPrint will also support to run under Python 3 in addition to the deprecated
# Python 2. New plugins should make sure to run under both versions for now. Uncomment one of the following
# compatibility flags according to what Python versions your plugin supports!
# __plugin_pythoncompat__ = ">=2.7,<3" # only python 2
# __plugin_pythoncompat__ = ">=3,<4"  # only python 3
__plugin_pythoncompat__ = ">=2.7,<4"  # python 2 and 3
